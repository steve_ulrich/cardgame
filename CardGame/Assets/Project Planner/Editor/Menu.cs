﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace ProjectPlanner
{
    sealed class Menu
    {
        private const string root = "Tools/Project Planner/";
        private const string help = root + "Help/";
        private const string import = root + "Import/";

        [MenuItem(root + "Board Window %&b", false, 0)]
        private static void OpenBoardWindow()
        {
            BoardWindow.Init();
        }
        [MenuItem(root + "Task Window %&t", false, 1)]
        private static void OpenTaskWindow()
        {
            TaskWindow.Init();
        }
        [MenuItem(root + "Quick Task %&q", false, 2)]
        private static void OpenQuickTaskWindow()
        {
            QuickTaskWindow.Init();
        }

        [MenuItem(root + "Settings", false, 99)]
        public static void OpenSettings()
        {
            PreferencesWindow.Init();
        }

        [MenuItem(import + "GitKraken Glo Boards", false, 100)]
        public static void GitKrakenGloBoards()
        {
            GitKrakenWindow.Init();
        }

        [MenuItem(help + "Welcome", false, 101)]
        private static void OpenWelcomeWindow()
        {
            WelcomeWindow.Init();
        }
        [MenuItem(help + "Demo Content", false, 102)]
        public static void DemoContent()
        {
            DemoImporter.Init();
        }
        [MenuItem(help + "Manual", false, 103)]
        public static void Manual()
        {
            Application.OpenURL(FileManager.ManualPath);
        }
        [MenuItem(help + "Release Notes", false, 104)]
        public static void OpenReleaseNotes()
        {
            ReleaseNotesWindow.Init();
        }
        [MenuItem(help + "Data Recovery", false, 114)]
        private static void RunAutomaticErrorFixing()
        {
            if (UI.Utilities.Dialog(Language.DataRecovery, Language.AreYouSureYouWantToRunDataRecovery, Language.Yes, Language.No))
            {
                DataRecovery.Run(DataRecovery.ErrorCode.User, false);
            }
        }
        [MenuItem(help + "Report Bug", false, 115)]
        private static void ReportIssue()
        {
            Application.OpenURL(Info.ReportBugURL);
        }
        [MenuItem(help + "Contact Us", false, 116)]
        private static void ContactUs()
        {
            Application.OpenURL(Info.ContactURL);
        }
        
        [MenuItem(root + "About", false, 103)]
        public static void OpenAbout()
        {
            AboutWindow.Init();
        }
    }
}
