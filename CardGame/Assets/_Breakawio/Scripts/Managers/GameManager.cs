﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullInspector;
using Kalard.GameStates;


namespace Kalard
{
    public enum Team
    {
        None = 0,
        Red = 1,
        Blue = 2,
        Spectator = 3
    }

    public class GameManager : MonoBehaviour
    {
        public PlayerHolder[] allPlayers;
        public PlayerStatsUI[] allStatsUI;

        public UnityEngine.UI.Button endTurnButton;

        [Space(10)]
        public State currentState;

        [Space(10)]
        public Team localTeam;

        [Space(10)]
        public PlayerHolder currentPlayer;

        public CardHolders playerOneHolder;
        public CardHolders playerOneAllyHolder;
        public CardHolders enemyHolder;
        public CardHolders enemyAllyHolder;

        [Space(10)]
        public GameObject cardPrefab;

        [Space(10)]
        public float maxTurnTime = 30f;
        public SO.FloatVariable currentTurnTime;

        [Space(10)]
        public int turnIndex;
        public Turn[] turns;

        [Space(10)]
        public SO.GameEvent onTurnChanged;
        public SO.GameEvent onPhaseChanged;

        [Space(10)]
        public SO.StringVariable turnText;

        [Space(10)]
        public LaneManager laneManager;

        private bool isInitialized;

        private void Awake()
        {
            Debug.Log("GameManager| Instance set");
            Settings.gameManager = this;
        }

        private void Update()
        {
            if (!isInitialized)
                return;

            bool isTurnComplete = turns[turnIndex].Execute();

            currentTurnTime.value += Time.deltaTime;

            if (currentTurnTime.value > maxTurnTime && !isTurnComplete)
            {
                isTurnComplete = true;
            }

            if (isTurnComplete)
            {
                laneManager.UpdateAllTerritoryMarkers(turns[turnIndex].player.team == localTeam);

                currentPlayer.statsUI.SetTurnActive(false);

                turnIndex++;
                if (turnIndex > turns.Length - 1)
                {
                    turnIndex = 0;
                }

                currentPlayer = turns[turnIndex].player;

                endTurnButton.interactable = currentPlayer.isLocalPlayer;

                currentPlayer.statsUI.SetTurnActive(true);

                // The current player has changed here...
                turns[turnIndex].OnTurnStart();
                laneManager.MoveAllCreaturesForTurnStart(currentPlayer);

                turnText.value = turns[turnIndex].player.userName;

                onTurnChanged.Raise();

                currentTurnTime.value = 0f;
            }

            if (currentState != null)
            {
                currentState.Tick(Time.deltaTime);
            }
        }

        #region Init

        public void InitGame(int startingPlayerId)
        {
            Debug.Log("Init Game!");
            ConfigureForNumberOfPlayers();

            SetupTurns(startingPlayerId);

            SetupPlayers();

            endTurnButton.interactable = currentPlayer.isLocalPlayer;
            turns[0].OnTurnStart();
            onTurnChanged.Raise();
            turnText.value = turns[turnIndex].player.userName;

            isInitialized = true;
        }

        void ConfigureForNumberOfPlayers()
        {
            if (allPlayers.Length <= 2)
            {
                // 2 player game, cut off our turns array
                Turn[] newTurns = new Turn[2];
                newTurns[0] = turns[0];
                newTurns[1] = turns[1];

                turns = newTurns;

                PlayerStatsUI[] newStatsUI = new PlayerStatsUI[2];
                newStatsUI[0] = allStatsUI[0];
                newStatsUI[1] = allStatsUI[1];

                allStatsUI[2].gameObject.SetActive(false);
                allStatsUI[3].gameObject.SetActive(false);

                allStatsUI = newStatsUI;

            }
        }

        void SetupTurns(int startingPlayerId)
        {
            // Get our starting player and assign them to turn0
            for (int i = 0; i < allPlayers.Length; i++)
            {
                if (allPlayers[i].gameId == startingPlayerId)
                {
                    turns[0].player = allPlayers[i];
                    Debug.Log("Local team T[0] set");
                    break;
                }
            }

            // Assign the remaining players to turns 2 and 4
            for (int i = 0; i < allPlayers.Length; i++)
            {
                if (allPlayers[i].isLocalPlayer)
                {
                    localTeam = allPlayers[i].team;
                    Debug.Log("Local team: " + localTeam.ToString());
                }
                if (turns.Length > 2 && allPlayers[i].team == turns[0].player.team && allPlayers[i] != turns[0].player)
                {
                    // assign to turn 3
                    turns[2].player = allPlayers[i];
                }
                if (allPlayers[i].team != turns[0].player.team)
                {
                    if (turns[1].player == null)
                    {
                        Debug.Log("Enemy team T[1] set");
                        turns[1].player = allPlayers[i];
                    }
                    // Check for solo match.
                    else if (turns.Length > 2 && turns[3].player == null)
                    {
                        Debug.Log("Enemy team T[3] set");
                        turns[3].player = allPlayers[i];
                    }
                }
            }

            // Current player should just be Mr. Turns[0]
            currentPlayer = turns[0].player;
        }

        void SetupPlayers()
        {
            for (int i = 0; i < allPlayers.Length; i++)
            {
                PlayerStatsUI statsUI = allStatsUI[0];

                if(allPlayers[i].isLocalPlayer)
                {
                    Debug.Log("player set -- " + allPlayers[i].userName);
                    allPlayers[i].currentHolder = playerOneHolder;
                    statsUI = allStatsUI[0];
                }
                if (!allPlayers[i].isLocalPlayer && allPlayers[i].team == localTeam)
                {
                    Debug.Log("playerAlly set -- " + allPlayers[i].userName);
                    allPlayers[i].currentHolder = playerOneAllyHolder;
                    statsUI = allStatsUI[2];
                }

                if (allPlayers[i].team != localTeam)
                {
                    Debug.Log("enemy set -- " + allPlayers[i].userName);
                    allPlayers[i].currentHolder = enemyHolder;
                    statsUI = allStatsUI[1];
                }

                allPlayers[i].Init();
                allPlayers[i].currentHolder.LoadPlayer(allPlayers[i], statsUI);
                allPlayers[i].LoadPlayerOnStatsUI();
            }

            turns[0].player.statsUI.SetTurnActive(true);
        }

        #endregion

        public bool IsLocalPlayerTurn()
        {
            return currentPlayer.isLocalPlayer;
        }

        public bool IsTeamTurn(Team team)
        {
            bool isTeamTurn = turns[turnIndex].player.team == team;
            Debug.Log("Is it " + team + " team's turn? " + isTeamTurn);
            return isTeamTurn;
        }

        public void SetState(State s)
        {
            currentState = s;

            if(currentState != null) currentState.OnStateEnter();
        }

        public void EndCurrentPhase()
        {
            SessionManager.instance.EndCurrentPhase();
        }

        public void EndTurn()
        {
            turns[turnIndex].EndCurrentPhase();
        }

        public void PickNewCardFromDeck(PlayerHolder p)
        {
            if(p.allCards.Count == 0)
            {
                Debug.Log(p.userName + " is out of cards!");
                return;
            }

            string cardId = p.allCards[0];
            p.allCards.RemoveAt(0); // Remove the picked card from our current copy of all our cards

            GameObject go = Instantiate(cardPrefab) as GameObject;
            CardUIControl ui = go.GetComponent<CardUIControl>();
            
            ui.LoadCard(Settings.ResourcesManager.CreateCardInstance(cardId));
            CardInstance inst = go.GetComponentInChildren<CardInstance>();
            inst.owner = p;
            inst.currentLogic = p.handLogic;
            Settings.SetParentForCard(go.transform, p.currentHolder.handGrid.value);

            p.handCards.Add(inst);
            if(!p.isLocalPlayer)
            {
                ui.cardRenderer.alpha = 0f;
            }
        }

        public void CreateResourceCards()
        {
            ResourcesManager rm = Settings.ResourcesManager;

            for (int i = 0; i < Settings.gameManager.currentPlayer.resourcesPerTurn; i++)
            {
                GameObject go = Instantiate(cardPrefab) as GameObject;

                CanvasGroup canvasGroup = go.GetComponent<CanvasGroup>();
                if (canvasGroup != null)
                {
                    canvasGroup.alpha = 0f;
                    canvasGroup.blocksRaycasts = false;
                }

                // @HERE -- Load the card's UI elements onto a card instance. Pretty unncessary
                // if we end up doing invisible resources
                CardUIControl ui = go.GetComponentInChildren<CardUIControl>();
                ui.LoadCard(rm.CreateCardInstance(currentPlayer.manaCard));

                CardInstance inst = go.GetComponentInChildren<CardInstance>();
                inst.currentLogic = currentPlayer.handLogic;

                // @HERE -- Change this to put the resource cards in the player's hands as playable
                // if we end up NOT wanting automatic resources...
                Settings.SetParentForCard(go.transform, currentPlayer.currentHolder.resourcesGrid.value);

                Settings.gameManager.currentPlayer.AddResourceCard(go);
            }
        }

        #region Unused

        public bool IsLocalTeamTurn()
        {
            bool isLocalTeamTurn = currentPlayer.team == localTeam;
            Debug.Log("Is it the local team's turn? " + isLocalTeamTurn);
            return isLocalTeamTurn;
        }

        public void LoadPlayerOnActive(PlayerHolder p)
        {
            if(playerOneHolder.playerHolder != p)
            {
                LoadPlayerOnHolder(p, playerOneHolder, allStatsUI[0]);
            }

        }

        public void LoadPlayerOnHolder(PlayerHolder p, CardHolders h, PlayerStatsUI statsUI)
        {
            h.LoadPlayer(p, statsUI);
        }
        #endregion


    }
}
