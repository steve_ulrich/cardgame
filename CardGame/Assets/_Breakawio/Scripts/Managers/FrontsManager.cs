﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    public class FrontsManager : MonoBehaviour
    {

        public TerritoryMarker leftLaneFront;
        public TerritoryMarker midLaneFront;
        public TerritoryMarker rightLaneFront;
    }

}
