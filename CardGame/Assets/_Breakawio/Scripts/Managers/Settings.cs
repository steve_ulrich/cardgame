﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

namespace Kalard
{
    public static class Settings
    {
        public static Kalard.GameManager gameManager;

        private static ResourcesManager _resourcesManager;

        public static ResourcesManager ResourcesManager
        {
            get
            {
                if (_resourcesManager == null)
                {
                    _resourcesManager = Resources.Load<ResourcesManager>("ResourcesManager");
                    _resourcesManager.Init();
                }

                return _resourcesManager;
            }
        }

        public static List<RaycastResult> GetUIObjects()
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current)
            {
                position = Input.mousePosition
            };

            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerData, results);
            return results;

        }

        public static void DropCreatureCard(Card card, TileControl tile)
        {
            GameObject prefab = card.SpawnPrefab();
            prefab.transform.SetParent(tile.transform);
            prefab.transform.localPosition = Vector3.zero;
            prefab.transform.rotation = Quaternion.identity;
            prefab.transform.localScale = Vector3.one;
            prefab.gameObject.SetActive(true);

            Creature creature = prefab.GetComponent<Creature>(); // Can assume this has creature component as a creature card
            if(creature == null)
            {
                Debug.LogError("No Creature component assigned to this prefab!");
            }

            creature.SetCreatureOwner(gameManager.currentPlayer);
            creature.OnSpawn(tile);

            CardProperties resourceProperty = card.GetCardProperty(Settings.ResourcesManager.resourceElement);
            int cardCost = resourceProperty.intValue;

            gameManager.currentPlayer.UseResourcesCards(cardCost);

        }

        public static void SetParentForCard(Transform card, Transform parent)
        {
            card.SetParent(parent);
            card.localPosition = Vector3.zero;
            card.localEulerAngles = Vector3.zero;
            card.localScale = Vector3.one;
        }
    }
}

