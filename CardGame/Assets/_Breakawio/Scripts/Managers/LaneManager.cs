﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    public class LaneManager : MonoBehaviour
    {
        public Lane leftLane;
        public Lane midLane;
        public Lane rightLane;

        public void UpdateAllTerritoryMarkers(bool isLocal)
        {
            leftLane.ResetTerritoryToFurthestControlledTile(isLocal);
            midLane.ResetTerritoryToFurthestControlledTile(isLocal);
            rightLane.ResetTerritoryToFurthestControlledTile(isLocal);
        }

        public void MoveAllCreaturesForTurnStart(PlayerHolder currentPlayer)
        {
            // Move left laners
            foreach(var tileList in leftLane.tiles)
            {
                for(int i = 0; i < tileList.Value.Count; i++)
                {
                    // Efficient check for the tile to be occupied by our team before we do some expensive getcomponent
                    if(tileList.Value[i].occupyingTeam == currentPlayer.team)
                    {
                        Creature c = tileList.Value[i].GetComponentInChildren<Creature>();
                        if (c == null)
                            continue;

                        if(c.owner == currentPlayer)
                        {
                            c.remainingMovement = 1;
                        }
                    }
                }
            }

            // Move mid laners
            foreach (var tileList in midLane.tiles)
            {
                for (int i = 0; i < tileList.Value.Count; i++)
                {
                    // Efficient check for the tile to be occupied by our team before we do some expensive getcomponent
                    if (tileList.Value[i].occupyingTeam == currentPlayer.team)
                    {
                        Creature c = tileList.Value[i].GetComponentInChildren<Creature>();
                        if (c == null)
                            continue;

                        if (c.owner == currentPlayer)
                        {
                            c.remainingMovement = 1;
                        }
                    }
                }
            }

            // Move right laners
            foreach (var tileList in rightLane.tiles)
            {
                for (int i = 0; i < tileList.Value.Count; i++)
                {
                    // Efficient check for the tile to be occupied by our team before we do some expensive getcomponent
                    if (tileList.Value[i].occupyingTeam == currentPlayer.team)
                    {
                        Creature c = tileList.Value[i].GetComponentInChildren<Creature>();
                        if (c == null)
                            continue;

                        if (c.owner == currentPlayer)
                        {
                            c.remainingMovement = 1;
                        }
                    }
                }
            }
        }
    }
}

