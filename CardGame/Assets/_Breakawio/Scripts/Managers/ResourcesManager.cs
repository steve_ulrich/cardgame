﻿using UnityEngine;
using System.Collections.Generic;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/ResourcesManager")]
    public class ResourcesManager : ScriptableObject
    {
        public Element typeElement;
        public Element resourceElement;
        public Element primaryStatElement;
        public Element secondaryStatElement;

        public Card[] allCards;

        public Dictionary<string, Card> cardsDict = new Dictionary<string, Card>();

        public void Init()
        {
            cardsDict.Clear();
            for (int i = 0; i < allCards.Length; i++)
            {
                cardsDict.Add(allCards[i].name, allCards[i]);
            }
        }

        public Card GetCardInstance(string id)
        {
            Card originalCard = GetCard(id);

            return originalCard;
        }

        public Card CreateCardInstance(string id)
        {
            Card originalCard = GetCard(id);
            if (originalCard == null)
            {
                return null;
            }

            Card newInstance = Instantiate(originalCard);
            newInstance.name = originalCard.name;
            return newInstance;
        }

        private Card GetCard(string id)
        {
            Card result = null;
            cardsDict.TryGetValue(id, out result);

            return result;
        }
    }

}
