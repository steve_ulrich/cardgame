﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Kalard
{
    public class PlayerStatsUI : MonoBehaviour
    {
        public PlayerHolder player;
        public Image turnIndicator;
        public Image playerPortrait;
        public TMPro.TextMeshProUGUI health;
        public TMPro.TextMeshProUGUI playerName;
        public TMPro.TextMeshProUGUI resources;

        private void Awake()
        {
            SetTurnActive(false);
        }

        public void UpdateAllUI()
        {
            UpdateUsername();
            UpdateHealth();
        }

        public void UpdateUsername()
        {
            playerName.text = player.userName;

            playerPortrait.sprite = player.portrait;
        }

        public void UpdateHealth()
        {
            health.text = player.health.ToString();
        }

        public void UpdateResources()
        {
            int remainingResources = player.NonUsedResources();

            resources.text =  remainingResources.ToString() + " / " + player.resourcesCount.ToString();
        }

        public void SetTurnActive(bool isTurn)
        {
            turnIndicator.enabled = isTurn;
        }
    }
}

