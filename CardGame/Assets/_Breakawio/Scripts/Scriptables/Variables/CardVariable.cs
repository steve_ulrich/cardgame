﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/Card Variable")]
    public class CardVariable : ScriptableObject
    {
        public Kalard.CardInstance value;

        public void Set(CardInstance v)
        {
            value = v;
        }
    }
}

