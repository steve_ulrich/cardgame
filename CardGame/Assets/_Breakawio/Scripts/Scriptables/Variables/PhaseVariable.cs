﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Variables/Phase")]
    public class PhaseVariable : ScriptableObject
    {
        public Phase value;
    }
}

