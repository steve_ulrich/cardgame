﻿
namespace SO.UI
{
    public class UpdateTextFromPhase : UIPropertyUpdater
    {
        public Kalard.PhaseVariable currentPhase;
        public TMPro.TextMeshProUGUI targetText;

        /// <summary>
        /// Use this to update a text UI element based on the target phase variable
        /// </summary>
        public override void Raise()
        {
            targetText.text = currentPhase.value.phaseName;
        }

    }
}

