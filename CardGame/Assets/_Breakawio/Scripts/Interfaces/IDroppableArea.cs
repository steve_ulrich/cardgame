﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    public interface IDroppableArea
    {
        void OnDrop();
    }
}

