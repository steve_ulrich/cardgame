﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    public class CurrentSelected : ObjectFollowMouse {

        public CardVariable currentCard;
        public CardUIControl cardUI;

        private void Start()
        {
            CloseCard();
        }

        public void LoadCard()
        {
            if (currentCard.value == null)
                return;

            currentCard.value.gameObject.SetActive(false);
            cardUI.LoadCard(currentCard.value.uIControl.card);

            cardUI.gameObject.SetActive(true);
        }

        public void CloseCard()
        {
            cardUI.gameObject.SetActive(false);
        }
    }
}

