﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Breakawio
{
    public class SceneManager : Singleton<SceneManager>
    {
        public void LoadScene(string scene, System.Action onSceneLoaded = null)
        {
            StartCoroutine(Kalard.Utility.LoadScene(scene, onSceneLoaded));
        }

    }
}


