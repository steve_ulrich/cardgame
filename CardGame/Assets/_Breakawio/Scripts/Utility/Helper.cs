﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    public static class Helper
    { 
        public static void Shuffle<T>(this List<T> deck)
        {
            var r = new System.Random();
            for(var n = deck.Count - 1; n > 0; --n)
            {
                var k = r.Next(n + 1);
                var temp = deck[n];
                deck[n] = deck[k];
                deck[k] = temp;
            }
        }
    }
}

