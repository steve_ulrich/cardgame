﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GenericPopup : MonoBehaviour
{
    public Animator Anim;

    public Text TitleText;
    public Text MessageText;

    public Button CancelButton;
    public Text CancelButtonText;
    public Button OkButton;
    public Text OkButtonText;

    private int closePopupAnimHash;

    private void OnEnable()
    {
        closePopupAnimHash = Animator.StringToHash("Fade-out");

        Anim.Play("Fade-in");

    }

    public void ClosePopup()
    {
        if(Anim != null)
        {
            Anim.Play(closePopupAnimHash);
        }

        Invoke("DestroyPopup", 1f);
    }

    public void DestroyPopup()
    {
        Destroy(this.gameObject);
    }

}
