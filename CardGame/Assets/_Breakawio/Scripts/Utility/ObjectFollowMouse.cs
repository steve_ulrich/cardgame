﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    public class ObjectFollowMouse : MonoBehaviour {

        private Transform m_Transform;

        private void Awake()
        {
            m_Transform = this.transform;
        }

        private void Update()
        {
            m_Transform.position = Input.mousePosition;
        }
    }
}
