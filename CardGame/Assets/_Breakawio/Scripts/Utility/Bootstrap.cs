﻿using System.Collections;
using UnityEngine;


/// <summary>
/// The bootstrap class handles initialization of various version/binary specific classes throughout the project.
/// 
/// It will be used to do internet connection tests
/// It will be used to load up dynamic properties from our cloud service that can be used by other classes at run-time
/// </summary>
public class Bootstrap : MonoBehaviour {

    public BootstrapLoadingUI BootstrapLoadingCanvas;

    private void Start()
    {
        StartCoroutine(BootstrapLoader());
    }

    IEnumerator BootstrapLoader()
    {

        // Connection Test
        BootstrapLoadingCanvas.LoadingBarImage.fillAmount = 0f;
        bool connectionTestIsComplete = false;
        bool hasConnection = false;
        // Setup a callback that will handle a true or false result from our internet connection test
        System.Action<bool> cbOnConnectionTestComplete = delegate (bool connectionStatus)
        {
            if (connectionStatus)
            {
                Debug.Log("Bootstrap| Connection Test Successful!");
                hasConnection = true;
            }
            else
            {
                Debug.Log("Bootstrap| ***Connection Test NOT Successful!");
                hasConnection = false;
            }

            connectionTestIsComplete = true;
        };

        Debug.Log("Bootstrap| Checking internet connection...");
        // Check for internet connectivity by downloading dynamic properties
        yield return (Kalard.DynamicProgramProperties.Initialize(cbOnConnectionTestComplete));

        float timer = 1f;
        while (connectionTestIsComplete == false)
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                Debug.Log("Bootstrap| Waiting for Dynamic Properties... something may be wrong ");
                timer = 1f;
            }
            yield return Kalard.Utility.WaitForFrame;
        }

        // If we have internet, download a set of dynamic program properties from the cloud
        if (hasConnection)
        {

            yield return MoveStatusBar(0.25f);

            Debug.Log("Bootstrap| Checking Dynamic Properties");

            bool shouldInitSRDebugger = false;
            Kalard.DynamicProgramProperties.TryGetValue<bool>("DebuggerEnabled", out shouldInitSRDebugger);

            // Enable SR Debugger on development builds ALWAYS
            if (UnityEngine.Debug.isDebugBuild)
            {
                Debug.Log("Bootstrap| Init SR Debugger");
                SRDebug.Init();
            }
            else // Otherwise check our DP to see if we should enable it for release builds
            {
                if (shouldInitSRDebugger)
                {
                    Debug.Log("Bootstrap| Force-Init SR Debugger");
                    SRDebug.Init();
                }
            }

            // @TODO: Get any asset bundles necessary here
            // Let's get character bundles when loading the game scene.
            // We should only get bundles related to the menu here...


            yield return MoveStatusBar(1f);
            yield return new WaitForSeconds(0.5f);
            Debug.Log("Bootstrap| Load complete");

        }
        // If we don't have internet, notify the user that this app requires internet etc etc.
        else
        {
            Kalard.Utility.MakeNewGenericPopup("ERROR", "Unable to connect to the internet. Please enable your connection and restart the app", false);
            Debug.Log("Error, no connection");
            yield break;
        }

        Kalard.Utility.CleanMemory();

        Breakawio.SceneManager.Instance.LoadScene("Login");

    }

    private IEnumerator MoveStatusBar(float targetValue)
    {
        float currentFill = BootstrapLoadingCanvas.LoadingBarImage.fillAmount;

        while (BootstrapLoadingCanvas.LoadingBarImage.fillAmount < targetValue)
        {
            currentFill += Time.deltaTime / 2;
            BootstrapLoadingCanvas.LoadingBarImage.fillAmount = Mathf.Lerp(currentFill, targetValue, currentFill / targetValue);

            yield return Kalard.Utility.WaitForFrame;
        }
    }
}
