﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/Actions/Player Actions/ LoadOnActiveHolder")]
    public class LoadOnActiveHolder : PlayerAction
    {
        public override void Execute(PlayerHolder player)
        {
            //Debug.Log("Load On Active Holder: " + player.userName);
            Settings.gameManager.LoadPlayerOnActive(player);
        }
    }

}
