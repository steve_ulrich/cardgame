﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/Actions/Player Actions/ PickCardFromDeck")]
    public class PickCardFromDeck : PlayerAction
    {
        public override void Execute(PlayerHolder player)
        {
            //Debug.Log("Load On Active Holder: " + player.userName);
            Settings.gameManager.PickNewCardFromDeck(player);
        }
    }
}
