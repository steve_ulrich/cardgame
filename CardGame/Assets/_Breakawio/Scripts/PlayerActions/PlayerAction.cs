﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    public abstract class PlayerAction : ScriptableObject
    {
        public abstract void Execute(PlayerHolder player);
    }
}

