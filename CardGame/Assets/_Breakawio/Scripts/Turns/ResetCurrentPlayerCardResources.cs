﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/Turns/ResetResourcePhase")]
    public class ResetCurrentPlayerCardResources : Phase
    {
        public override bool IsComplete()
        {

            // This is where we increment the player's resource cards every round of turns
            Settings.gameManager.CreateResourceCards();

            Settings.gameManager.currentPlayer.MakeAllResourcesCardsUsable();

            return true;
        }

        public override void OnEndPhase()
        {
            
        }

        public override void OnStartPhase()
        {
            
        }
    }
}
