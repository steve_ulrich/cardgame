﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/Turns/Turn")]
    public class Turn : ScriptableObject
    {
        public PlayerHolder player;
        [System.NonSerialized]
        public int index;
        public PhaseVariable currentPhase;
        public Phase[] phases;

        public PlayerAction[] turnStartActions;

        public void OnTurnStart()
        {
            if(turnStartActions == null)
            {
                Debug.Log("No Turn Start Actions");
                return;
            }

            //Debug.Log("Executing " + turnStartActions.Length + " turn starts");
            foreach(var action in turnStartActions)
            {
                action.Execute(player);
            }
        }

        public bool Execute()
        {
            bool result = false;

            currentPhase.value = phases[index];
            phases[index].OnStartPhase();

            bool phaseIsComplete = phases[index].IsComplete();

            if(phaseIsComplete)
            {
                phases[index].OnEndPhase();
                index++;
                if(index > phases.Length - 1)
                {
                    index = 0;
                    result = true;
                }

            }

            return result;

        }

        public void EndCurrentPhase()
        {
            phases[index].forceExit = true;
        }
    }
}

