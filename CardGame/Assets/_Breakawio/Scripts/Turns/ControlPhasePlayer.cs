﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/Turns/Control Phase Player")]
    public class ControlPhasePlayer : Phase
    {
        public GameStates.State playerControlState;

        public override bool IsComplete()
        {
            if(forceExit)
            {
                forceExit = false;
                return true;
            }
            // player pressed end turn or timer has run out
            return false;
        }

        public override void OnEndPhase()
        {
            if (isInitialized)
            {
                Settings.gameManager.SetState(null);
                isInitialized = false;
            }
        }

        public override void OnStartPhase()
        {
            if (!isInitialized)
            {
                Settings.gameManager.SetState(playerControlState);
                Settings.gameManager.onPhaseChanged.Raise();
                isInitialized = true;
            }
        }
    }
}
