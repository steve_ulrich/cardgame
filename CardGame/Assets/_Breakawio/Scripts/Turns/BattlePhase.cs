﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/Turns/Battle Phase Player")]
    public class BattlePhase : Phase
    {
        public override bool IsComplete()
        {
            if (forceExit)
            {
                forceExit = false;
                return true;
            }

            return false;
        }

        public override void OnEndPhase()
        {
            if (isInitialized)
            {
                Settings.gameManager.SetState(null);
                isInitialized = false;
            }
        }

        public override void OnStartPhase()
        {
            if (!isInitialized)
            {
                Settings.gameManager.SetState(null);
                Settings.gameManager.onPhaseChanged.Raise();
                isInitialized = true;
            }
        }
    }
}
