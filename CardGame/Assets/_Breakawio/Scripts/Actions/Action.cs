﻿using UnityEngine;
using System.Collections;

namespace Kalard.GameStates
{
    public abstract class Action : ScriptableObject
    {
        public abstract void Execute(float delta);
        public virtual void OnStateEnter()
        {

        }
    }
}

