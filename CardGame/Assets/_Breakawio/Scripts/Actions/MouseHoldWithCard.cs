﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Kalard.GameStates
{
    [CreateAssetMenu(menuName = "Kalard/Actions/MouseHoldWithCard")]
    public class MouseHoldWithCard : Action
    {
        public CardVariable currentCard;
        public State playerControlState;
        public SO.GameEvent onPlayerControlState;

        private CurrentSelected currentSelected;
        private bool isInDroppableArea = false;

        public override void OnStateEnter()
        {
            if(currentSelected != null)
            {
                return;
            }

            currentSelected = GameObject.Find("CurrentSelectedCard").GetComponent<CurrentSelected>();
        }


        public override void Execute(float delta)
        {
            bool mouseIsDown = Input.GetMouseButton(0);

            if (!isInDroppableArea)
            {
                currentSelected.cardUI.SetCardFade(1f);
            }

            List<RaycastResult> results = Settings.GetUIObjects();

            IDroppableArea dArea = null;

            foreach (var r in results)
            {
                // Check for droppable areas
                dArea = r.gameObject.GetComponentInParent<IDroppableArea>();
                if (dArea != null)
                {
                    if(!isInDroppableArea)
                    {
                        currentSelected.cardUI.FadeCard(0f);
                    }

                    isInDroppableArea = true;
                    break;
                }
                else
                {
                    isInDroppableArea = false;
                }
            }

            if (!mouseIsDown) // They released the mouse button
            {
                if(isInDroppableArea)
                {
                    dArea.OnDrop();
                }
                else
                {
                    currentCard.value.gameObject.SetActive(true);
                }

                isInDroppableArea = false;
                currentCard.value = null;
                currentSelected.cardUI.SetCardFade(1);

                Settings.gameManager.SetState(playerControlState);
                onPlayerControlState.Raise();

                return;
            }
        }
    }
}

