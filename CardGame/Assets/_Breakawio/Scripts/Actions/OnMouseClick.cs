﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using Kalard.GameStates;

namespace Kalard
{

    [CreateAssetMenu(menuName = "Kalard/Actions/OnMouseClick")]
    public class OnMouseClick : Action
    {
        public override void Execute(float delta)
        {
            if(Input.GetMouseButtonDown(0))
            {
                PointerEventData pointerData = new PointerEventData(EventSystem.current)
                {
                    position = Input.mousePosition
                };

                List<RaycastResult> results = new List<RaycastResult>();
                EventSystem.current.RaycastAll(pointerData, results);

                Kalard.IClickable c = null;

                foreach (var result in results)
                {
                    c = result.gameObject.GetComponentInParent<Kalard.IClickable>();
                    if (c != null)
                    {
                        c.OnClick();
                        break;
                    }
                }
            }
        }
    }
}

