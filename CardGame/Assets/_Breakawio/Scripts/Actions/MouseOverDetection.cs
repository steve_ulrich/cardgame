﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using Kalard.GameStates;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/Actions/MouseOverDetection")]
    public class MouseOverDetection : Action
    {
        Kalard.IClickable currentHighlight;
        public override void Execute(float delta)
        {
            List<RaycastResult> results = Settings.GetUIObjects();
        
            foreach (var result in results)
            {
                Kalard.IClickable highlightable = result.gameObject.GetComponentInParent<Kalard.IClickable>();
            
                if (highlightable != null)
                {
                    if(highlightable != currentHighlight)
                    {
                        if (currentHighlight != null)
                        {
                            currentHighlight.OnStopHighlight();
                        }
                    }
                    currentHighlight = highlightable;

                    currentHighlight.OnHighlight();
                    break;
                }
                else
                {
                    if (currentHighlight != null)
                    {
                        currentHighlight.OnStopHighlight();
                    }
                }
            }
        }
    }
}
