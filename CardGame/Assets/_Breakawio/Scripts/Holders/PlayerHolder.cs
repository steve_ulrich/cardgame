﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/Holders/Player Holder")]
    public class PlayerHolder : ScriptableObject {

        // Networking vars
        public bool isLocalPlayer;
        [System.NonSerialized]
        public int gameId = -1; // Used to identify the player (mostly for Networking as the PhotonId)

        // Gameplay vars
        public int resourcesCount
        {
            get { return currentHolder.resourcesGrid.value.GetComponentsInChildren<CardUIControl>().Length; }
        }

        public string manaCard;

        public Team team;

        public string userName;
        public Sprite portrait;
        public List<string> deckCards = new List<string>();
        [System.NonSerialized]
        public List<string> allCards = new List<string>();

        public int health = 20;

        public PlayerStatsUI statsUI;

        public int resourcesPerTurn = 1;


        [System.NonSerialized]
        public int resourcesDroppedThisTurn;

        public GameLogic.BaseActorLogic handLogic;
        public GameLogic.BaseActorLogic downLogic;

        

        [System.NonSerialized]
        public CardHolders currentHolder;

        [System.NonSerialized]
        public List<CardInstance> handCards = new List<CardInstance>();
        [System.NonSerialized]
        public List<CardInstance> cardsDown = new List<CardInstance>();
        [System.NonSerialized]
        public List<ResourceHolder> resourcesList = new List<ResourceHolder>();

        public void Init()
        {
            health = 20;

            allCards.AddRange(deckCards);

            allCards.Shuffle();
        }

        public void AddResourceCard(GameObject cardObject)
        {
            ResourceHolder r = new ResourceHolder()
            {
                cardObj = cardObject
            };

            resourcesList.Add(r);

            resourcesDroppedThisTurn++;
        }

        public int NonUsedResources()
        {
            int result = 0;

            for(int i = 0; i < resourcesList.Count; i++)
            {
                if(!resourcesList[i].isUsed)
                {
                    result++;
                }
            }

            return result;
        }

        public List<ResourceHolder> GetUnusedResources()
        {
            List<ResourceHolder> result = new List<ResourceHolder>();

            for(int i = 0; i < resourcesList.Count; i++)
            {
                if(!resourcesList[i].isUsed)
                {
                    result.Add(resourcesList[i]);
                }
            }

            return result;
        }

        public void MakeAllResourcesCardsUsable()
        {
            for(int i = 0; i < resourcesList.Count; i++)
            {
                resourcesList[i].isUsed = false;
                resourcesList[i].cardObj.transform.localEulerAngles = Vector3.zero;
            }

            statsUI.UpdateResources();
        }

        /// <summary>
        /// Check our resources against the card's cost to see if the player can play this card...
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
        public bool CanUseCard(Card card)
        {
            bool result = false;

            if(card.cardType is CreatureCard || card.cardType is SpellCard)
            {
                int currentResources = NonUsedResources();
                CardProperties resourceProperty = card.GetCardProperty(Settings.ResourcesManager.resourceElement);
                int cardCost = resourceProperty.intValue;

                if (cardCost <= currentResources)
                {
                    result = true;
                }
            }
            else
            {
                if(card.cardType is ResourceCard)
                {
                    if(resourcesPerTurn - resourcesDroppedThisTurn > 0)
                    {
                        result = true;
                    }
                }
            }

            return result;
        }

        public void UseResourcesCards(int amount)
        {
            Vector3 rotEuler = new Vector3(0f, 0f, 90f);

            List<ResourceHolder> unusedResources = GetUnusedResources();

            for (int i = 0; i < amount; i++)
            {
                unusedResources[i].isUsed = true;
                unusedResources[i].cardObj.transform.localEulerAngles = rotEuler;
            }

            statsUI.UpdateResources();
        }

        public void LoadPlayerOnStatsUI()
        {
            if(statsUI != null)
            {
                statsUI.player = this;
                statsUI.UpdateAllUI();

            }
        }

        public void DoDamage(int d)
        {
            health -= d;

            if(statsUI != null)
            {
                statsUI.UpdateHealth();
            }
        }
    }
}
