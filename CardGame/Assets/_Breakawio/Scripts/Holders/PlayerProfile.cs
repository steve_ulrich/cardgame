﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/PlayerProfile")]
    public class PlayerProfile : ScriptableObject
    {
        public string[] cardIds;

        public Sprite profileSprite;
    }

}
