﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/Holders/CardHolders")]
    public class CardHolders : ScriptableObject
    {
        public SO.TransformVariable handGrid;
        public SO.TransformVariable resourcesGrid;
        public SO.TransformVariable downGrid;

        [System.NonSerialized]
        public PlayerHolder playerHolder;

        public void LoadPlayer(PlayerHolder player, PlayerStatsUI statsUI)
        {
            if(player == null)
            {
                return;
            }

            player.currentHolder = this;
            playerHolder = player;

            foreach (var card in player.resourcesList)
            {
                card.cardObj.transform.SetParent(resourcesGrid.value);
            }

            foreach (var card in player.handCards)
            {
                card.uIControl.transform.SetParent(handGrid.value);
            }

            foreach (var card in player.cardsDown)
            {
                card.uIControl.transform.SetParent(downGrid.value);
            }

            player.statsUI = statsUI;
            player.LoadPlayerOnStatsUI();

        }
    }
}

