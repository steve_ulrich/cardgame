﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    public class TurnTimerUI : MonoBehaviour
    {
        public SO.FloatVariable currentTimer;
        public UnityEngine.UI.Image timerImage;

        private void Update()
        {
            float timePercentage = 1f - (currentTimer.value / Kalard.Settings.gameManager.maxTurnTime);

            timerImage.fillAmount = timePercentage;
        }

    }

}
