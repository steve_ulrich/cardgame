﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    public class PlayerUI : MonoBehaviour
    {
        public PlayerStatsUI playerUIPrefab;
        public Transform LocalPlayerHolder;

        public void CreatePlayerOnUI(PlayerHolder player)
        {
            PlayerStatsUI playerUI = Instantiate(playerUIPrefab);
            playerUI.transform.SetParent(LocalPlayerHolder, false);

            
        }
}

}
