﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseNetworkBehaviour : MonoBehaviour {

    public float updateRate = 0.1f;
    protected WaitForSeconds updateWait;

    protected virtual void Awake()
    {
        updateWait = new WaitForSeconds(updateRate);
    }
}
