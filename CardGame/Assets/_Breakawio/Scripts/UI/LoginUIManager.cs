﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginUIManager : MonoBehaviour
{
    public UnityEngine.UI.InputField displayName;
    public UnityEngine.UI.InputField emailAddress;
    public UnityEngine.UI.InputField passwordField;

    public GameObject LoginUIParent;

    private void Awake()
    {
        LoginUIParent.SetActive(false);

    }

    void Start()
    {
        // Attempt to login the last user @TODO Please don't save password in playerprefs
        if (PlayerPrefs.HasKey("LastUserDisplayName") && PlayerPrefs.HasKey("LastUserEmail") && PlayerPrefs.HasKey("LastUserPassword"))
        {
            GetLastUser();
        }
        else
        {
            LoginUIParent.SetActive(true);
        }
    }

    private void GetLastUser()
    {
        string email = PlayerPrefs.GetString("LastUserEmail");
        string pwd = PlayerPrefs.GetString("LastUserPassword");
        string displayName = PlayerPrefs.GetString("LastUserDisplayName");

        User.ActiveUser = new User(displayName, email, pwd, LoginComplete);
    }

    private void LoginComplete()
    {
        // Test if we should try to rejoin a game
        bool rejoinInProgress = false;
        if(rejoinInProgress)
        {
            Breakawio.SceneManager.Instance.LoadScene("Game");

        }
        else // if not, load the menu
        {
            Breakawio.SceneManager.Instance.LoadScene("MainMenu");
        }

    }

    public void OnClickLogin()
    {
        Debug.Log("Login Button Pressed");
        StartCoroutine(LoginTask());
    }

    IEnumerator LoginTask()
    {
        Debug.Log("----------Login Coroutine Start----------");
        yield return null;
        RegistrationRequest();

    }

    void RegistrationRequest()
    {
        Debug.Log("LoginUIManager| Registration Request");

        string email = emailAddress.text;
        string pwd = passwordField.text;
        string displayName = this.displayName.text;

        User.ActiveUser = new User(displayName, email, pwd, LoginComplete);
    }

}
