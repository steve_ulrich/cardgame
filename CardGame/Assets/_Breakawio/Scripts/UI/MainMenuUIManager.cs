﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Kalard
{

    public class MainMenuUIManager : MonoBehaviour
    {
        void OnDestroy()
        {

        }


        #region ButtonPressCallbacks

        //public float ease = 1f;
        //public Ease easMode;
        public void OnPressPlay()
        {
            Debug.Log("MainMenu| Play Button Pressed");

            // Animate in the PlayOptions panel
        }

        public void OnPressCustomize()
        {
            Debug.Log("MainMenu| Customize Button Pressed");
        }

        public void OnPressStore()
        {
            Debug.Log("MainMenu| Store Button Pressed");
        }

        public void OnPressLeaderboards()
        {
            Debug.Log("MainMenu| Leaderboards Button Pressed");
        }

        public void OnPressCasual(bool isOn)
        {
            Debug.Log("MainMenu| Casual Queue Button Pressed");

            // Enable/Animate Unranked Animation Screen

            if(isOn)
                Networking.NetworkManager.instance.matchmakingType = Networking.MatchmakingType.Casual;
        }

        public void OnPressRanked(bool isOn)
        {
            Debug.Log("MainMenu| Ranked Queue Button Pressed");
            if(isOn)
                Networking.NetworkManager.instance.matchmakingType = Networking.MatchmakingType.Ranked;
        }

        public void OnPressSolo(bool isOn)
        {
            Debug.Log("MainMenu| Solo Queue Button Pressed");
            if (isOn)
                Networking.NetworkManager.instance.queueType = Networking.QueueType.Solo;
        }

        public void OnPressDuo(bool isOn)
        {
            Debug.Log("MainMenu| Duo Queue Button Pressed");
            if (isOn)
                Networking.NetworkManager.instance.queueType = Networking.QueueType.Duo;
        }

        public void OnPressTournament()
        {
            Debug.Log("MainMenu| Tournament Queue Button Pressed");
        }

        public void OnPressCustom()
        {
            Debug.Log("MainMenu| Custom Button Pressed");
        }

        public void OnPressSettings()
        {
            Debug.Log("MainMenu| Custom Button Pressed");
        }

        public void OnPressQuit()
        {
            Debug.Log("MainMenu| Quit Button Pressed");

            Application.Quit();
        }

        public void OnPressParty1()
        {
            Debug.Log("MainMenu| Party1 Button Pressed");

        }

        public void OnPressParty2()
        {
            Debug.Log("MainMenu| Party2 Button Pressed");
        }

        public void OnPressAccount()
        {
            Debug.Log("MainMenu| Account Button Pressed");
        }

        public void OnPressCancelQueue()
        {
            Breakawio.SceneManager.Instance.LoadScene("MainMenu");
        }

        #endregion
    }
}
