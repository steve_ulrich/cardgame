﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CardUIProperties
{
    public TMPro.TextMeshProUGUI text;
    public UnityEngine.UI.Image image;
    public Element element;
    
}
