﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    public abstract class CardType : ScriptableObject
    {
        public string typeName;
        public virtual void OnSetType(CardUIControl ui)
        {
            Element t = Settings.ResourcesManager.typeElement;
            CardUIProperties typeProperty = ui.GetUIProperty(t);
            typeProperty.text.text = typeName;
        }
	
    }
}
