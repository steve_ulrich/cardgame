﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/CardTypes/Creature")]
    public class CreatureCard : CardType
    {
        public override void OnSetType(CardUIControl ui)
        {
            base.OnSetType(ui);
            ui.statsHolder.SetActive(true);
        }
    }
}
