﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    [CreateAssetMenu(fileName = "Card", menuName = "Kalard/Card", order = 1)]
    public class Card : ScriptableObject
    {
        [System.NonSerialized]
        public int instId;
        [System.NonSerialized]
        public CardUIControl cardUI;

        public GameObject spawnablePrefab;

        public CardType cardType;
        public CardProperties[] properties;

        public GameObject SpawnPrefab()
        {
            GameObject spawned = Instantiate(spawnablePrefab);

            return spawned;
        }

        public CardProperties GetCardProperty(Element e)
        {
            CardProperties result = null;

            for (int i = 0; i < properties.Length; i++)
            {
                if (properties[i].element == e)
                {
                    result = properties[i];
                    break;
                }
            }

            return result;
        }
    }
}

