﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Kalard
{
    public class CardUIControl : MonoBehaviour
    {
        public CardUIProperties[] properties;
        public Card card;
        public GameObject statsHolder;
        public GameObject resourceHolder;

        public CanvasGroup cardRenderer;
        public float cardFadeTime = 0.5f;

        public RectTransform m_rectTransform;
        Vector3 m_basePosition;

        private void Awake()
        {
            if (cardRenderer == null)
            {
                cardRenderer = GetComponent<CanvasGroup>();
            }

            m_basePosition = m_rectTransform.localPosition;
        }

        public void PullUpCard()
        {
            Vector3 pulledUpPosition = m_basePosition;
            pulledUpPosition.y += m_rectTransform.rect.height * 2.25f;

            m_rectTransform.localPosition = pulledUpPosition;
        }

        public void SendCardBackToHand()
        {
            m_rectTransform.localPosition = m_basePosition;
        }

        void OnStartDragCard()
        {

        }

        public void SetCardFade(float alpha)
        {
            cardRenderer.alpha = alpha;
        }

        public void FadeCard(float targetAlpha)
        {
            //Debug.Log("Fade Card");
            StartCoroutine(CardFader(targetAlpha));
        }

        private IEnumerator CardFader(float targetAlpha)
        {
            float startAlpha = cardRenderer.alpha;
            float currentTimer = 0f;


            while (currentTimer < cardFadeTime)
            {
                cardRenderer.alpha = Mathf.MoveTowards(startAlpha, targetAlpha, currentTimer / cardFadeTime);
                currentTimer += Time.deltaTime;
                yield return null;
            }

            cardRenderer.alpha = targetAlpha;
        }

        public void LoadCard(Card c)
        {
            if (c == null)
            {
                return;
            }

            CloseAll();

            card = c;

            c.cardUI = this;

            c.cardType.OnSetType(this);

            for (int i = 0; i < c.properties.Length; i++)
            {
                CardProperties cp = c.properties[i];

                CardUIProperties p = GetUIProperty(cp.element);

                if (p == null)
                    continue;

                if (cp.element is ElementText)
                {
                    p.text.text = cp.stringValue;
                    p.text.gameObject.SetActive(true);
                }
                else if (cp.element is ElementImage)
                {
                    p.image.sprite = cp.spriteValue;
                    p.image.gameObject.SetActive(true);
                }
                else if (cp.element is ElementInt)
                {
                    p.text.text = cp.intValue.ToString();
                    p.text.gameObject.SetActive(true);
                }
            }

        }

        public void CloseAll()
        {
            foreach (CardUIProperties p in properties)
            {
                if (p.image != null)
                {
                    p.image.gameObject.SetActive(false);
                }
                if (p.text != null)
                {
                    p.text.gameObject.SetActive(false);
                }
            }
        }

        public CardUIProperties GetUIProperty(Element e)
        {
            CardUIProperties result = null;

            for (int i = 0; i < properties.Length; i++)
            {
                if (properties[i].element == e)
                {
                    result = properties[i];
                    break;
                }
            }

            return result;
        }
    }

}