﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CardProperties
{
    public string stringValue;
    public int intValue;
    public Sprite spriteValue;
    public Element element;
}
