﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/CardTypes/Spell")]
    public class SpellCard : CardType
    {
        public override void OnSetType(CardUIControl ui)
        {
            base.OnSetType(ui);
            ui.statsHolder.SetActive(false);
        }
    }

}
