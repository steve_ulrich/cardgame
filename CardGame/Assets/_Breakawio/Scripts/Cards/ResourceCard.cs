﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/CardTypes/Resource")]
    public class ResourceCard : CardType
    {
        public override void OnSetType(CardUIControl ui)
        {
            base.OnSetType(ui);

            ui.resourceHolder.SetActive(false);
            ui.statsHolder.SetActive(false);
        }
    }

}