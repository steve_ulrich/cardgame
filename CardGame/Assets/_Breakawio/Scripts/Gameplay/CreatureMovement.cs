﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    public class CreatureMovement : MonoBehaviour
    {
        public void Move(Vector3 targetPos, System.Action onMoveComplete)
        {
            StartCoroutine(PerformMovement(targetPos, onMoveComplete));
        }

        IEnumerator PerformMovement(Vector3 targetPos, System.Action onMoveComplete)
        {
            yield return StartCoroutine(Utility.MoveTransformAsync(transform, targetPos, .5f));

            if(onMoveComplete != null)
            {
                onMoveComplete.Invoke();
            }
        }
    }

}
