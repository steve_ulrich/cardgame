﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kalard;

namespace Kalard
{

    public class DeckManager : Singleton<DeckManager>
    {
        public List<Card> OriginalDeckList = new List<Card>();
        public Stack<Card> Deck = new Stack<Card>();
        public List<Card> Hand = new List<Card>();

        protected override void Awake()
        {
            base.Awake();

            ResetDeck();
        }

        private void ResetDeck()
        {
            OriginalDeckList.Shuffle();

            foreach(var t in OriginalDeckList)
            {
                Deck.Push(t);
            }
        }

    }
}
