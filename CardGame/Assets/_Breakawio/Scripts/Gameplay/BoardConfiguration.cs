﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[CreateAssetMenu(fileName = "BoardConfig", menuName = "Kalard/Board", order = 1)]
public class BoardConfiguration : ScriptableObject
{
    public int LeftLaneRows;
    public int LeftLaneColumns;

    public int MiddleLaneRows;
    public int MiddleLaneColumns;

    public int RightLaneRows;
    public int RightLaneColumns;

    public GameObject TilePrefab;   //@TODO Get rid of this

    public IEnumerator SetupBoard(Transform boardParent)
    {
        Vector3 MidPosition = Vector3.zero;
        Vector3 LeftLanePosition = new Vector3(-Mathf.Floor(MiddleLaneColumns / 2) - 4 - Mathf.Floor(LeftLaneColumns / 2), 0, 0);
        Vector3 RightLanePosition = new Vector3(Mathf.Floor(MiddleLaneColumns / 2) + 4 + Mathf.Floor(RightLaneColumns / 2), 0, 0);

        // 5 rows
        // -2 to 2
        // 5 / 2 floor = 2
        // -2 to 2

        // 6 rows
        // -3 to 2

        // odd numbers take the floor of half the number of rows/columns and subtract it from 0 for the min, subtract it from the number of rows/columns for the max
        // even numbers take half the number of rows/columns and subtract it from 0 for the min, and 1 less than half and subtract it from the number of rows/columns for the max

        yield return SetupLane(LeftLanePosition, LeftLaneRows, LeftLaneColumns, boardParent);
        yield return SetupLane(MidPosition, MiddleLaneRows, MiddleLaneColumns, boardParent);
        yield return SetupLane(RightLanePosition, RightLaneRows, RightLaneColumns, boardParent);


    }

    IEnumerator SetupLane(Vector3 startPos, int rows, int columns, Transform parent)
    {
        int rowMin = Kalard.Utility.GetMinNumberSplit(rows);
        int rowMax = Kalard.Utility.GetMaxNumberSplit(rows);
        int colMin = Kalard.Utility.GetMinNumberSplit(columns);
        int colMax = Kalard.Utility.GetMaxNumberSplit(columns);

        for (int lRow = rowMin; lRow <= rowMax; lRow++)
        {
            for (int lCol = colMin; lCol <= colMax; lCol++)
            {
                Vector3 tilePos = startPos;
                tilePos.x += lCol;
                tilePos.z += lRow;
                Instantiate(TilePrefab, tilePos, Quaternion.Euler(90, 0, 0), parent);
                yield return null;
            }
        }
    }
}
