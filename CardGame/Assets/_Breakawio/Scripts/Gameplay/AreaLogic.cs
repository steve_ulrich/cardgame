﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    public abstract class AreaLogic : ScriptableObject
    {
        public abstract void Execute();
    }
}
