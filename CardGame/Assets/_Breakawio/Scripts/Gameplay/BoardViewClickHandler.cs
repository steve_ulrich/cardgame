﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BoardViewClickHandler : MonoBehaviour, IPointerClickHandler
{
    public UnityEngine.Events.UnityEvent OnClick;

    public void OnPointerClick(PointerEventData eventData)
    {
        OnClick.Invoke();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawCube(transform.position, new Vector3(transform.localScale.x, transform.localScale.z, transform.localScale.y));
    }
}
