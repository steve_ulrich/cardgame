﻿using UnityEngine;
using System.Collections;

namespace Kalard.GameStates
{
    [CreateAssetMenu(menuName = "State")]
    public class State : ScriptableObject
    {
        public Action[] actions;

        public void Tick(float deltaTime)
        {
            for(int i = 0; i < actions.Length; i++)
            {
                actions[i].Execute(deltaTime);
            }
        }

        public void OnStateEnter()
        {
            for (int i = 0; i < actions.Length; i++)
            {
                actions[i].OnStateEnter();
            }
        }
    }
}

