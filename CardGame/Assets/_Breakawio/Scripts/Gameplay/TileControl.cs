﻿using UnityEngine;

namespace Kalard
{
    public class TileControl : MonoBehaviour
    {
        public GameObject highlight;
        public Team occupyingTeam;

        [System.NonSerialized]
        public Lane lane;

        [System.NonSerialized]
        public int row;
        [System.NonSerialized]
        public int column;

    }

}
