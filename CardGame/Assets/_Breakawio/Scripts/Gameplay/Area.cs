﻿using UnityEngine;
using System.Collections;

namespace Kalard.GameLogic
{ 
    public class Area : MonoBehaviour, IDroppableArea
    {
        public AreaLogic logic;

        public void OnDrop()
        {
            //Debug.Log("On Drop");
            logic.Execute();
        }
    }
}
