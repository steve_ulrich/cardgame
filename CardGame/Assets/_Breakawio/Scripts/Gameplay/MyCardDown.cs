﻿using UnityEngine;
using System.Collections;

namespace Kalard.GameLogic
{
    [CreateAssetMenu(menuName = "Kalard/MyCardDown")]
    public class MyCardDown : Kalard.GameLogic.BaseActorLogic
    {
        public override void OnClick(CardInstance inst)
        {
            Debug.Log("this card is mine but on the table");
        }

        public override void OnHighlight(CardInstance inst)
        {

        }

        public override void OnStopHighlight(CardInstance inst)
        {

        }
    }
}
