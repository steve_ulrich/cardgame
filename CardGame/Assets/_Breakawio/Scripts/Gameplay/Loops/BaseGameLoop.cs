﻿
namespace Kalard
{
    public abstract class BaseGameLoop : UnityEngine.ScriptableObject
    {
        public abstract void Init();

        public abstract void Execute();
    }

}
