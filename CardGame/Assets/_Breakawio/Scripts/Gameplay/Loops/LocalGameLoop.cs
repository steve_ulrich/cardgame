﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    [UnityEngine.CreateAssetMenu(menuName = "Kalard/GameLoops/LocalGameLoop")]
    public class LocalGameLoop : BaseGameLoop
    {
        public int numPlayers;

        public override void Execute()
        {

        }

        public override void Init()
        {

        }
    }
}
