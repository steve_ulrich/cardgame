﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    /// <summary>
    /// This class exists on our Creature prefab, which is spawned when our Creature Card is played.
    /// </summary>
    public class Creature : MonoBehaviour
    {
        public enum CreatureState
        {
            IDLE,
            MOVING,
            FIGHTING,
            DEAD
        }

        public Card m_card; // Inspector Set
        public CreatureMovement movement;
        public CreatureCombat combat;

        [System.NonSerialized]
        public CreatureState currentState;
        
        [System.NonSerialized]
        public TileControl currentTile; // Set when we drop our creature card (on spawn)
        public TileControl targetTile; // Set every turn / every movement

        public Team team;

        public PlayerHolder owner;

        public List<GameObject> bodies;

        public int primaryStat;
        private int secondaryStat;

        [System.NonSerialized]
        public int remainingMovement = 0;

        private bool isBusy = false;


        private void Update()
        {
            if(isBusy)
            {
                return;
            }

            switch(currentState)
            {
                case CreatureState.IDLE:
                    if(remainingMovement > 0)
                    {
                        targetTile = GetForwardTile(currentTile.row);
                        if (targetTile != null)
                        {
                            Move();
                        }
                        else
                        {
                            // We reached the end of the board, do some daymage.
                            Debug.Log("Creature| Reached the enemy player, do some damage!");
                            Destroy(this.gameObject);
                        }
                    }
                    break;
                case CreatureState.FIGHTING:
                    Fight(targetTile);
                    break;
                case CreatureState.MOVING:
                    Move();
                    break;
                case CreatureState.DEAD:
                    Destroy(this.gameObject); // @TODO: Disable this object instead
                    break;
            }
        }

        // 1. Spawn Unit
        //      1a. OnSpawn();
        //      1b. CheckSurroundingTiles() for an enemy
        //          --Have a team property on the tile we can check without having to know about the creature?
        //          1i. Left
        //          1ii. Right
        //          1iii. Front
        // 2. If there is an enemy, make them our fight target.
        // 3. Fight().
        // 4. MoveToTarget().
        //      4a. Did we just fight?
        //          5i. Move to the tile we just fought over
        //      4b. Otherwise, move forward

        [ContextMenu("SETUP_TEST_CREATURE")]
        public void SetupTestCreature()
        {
                TileControl tile = GetComponentInParent<TileControl>();
                currentTile = tile;
                tile.occupyingTeam = team;

                currentState = CreatureState.IDLE;

                remainingMovement = 0;
            
        }

        public void SetCreatureOwner(PlayerHolder player)
        {
            owner = player;
            team = player.team;
        }

        // Do stuff like initialize the creature's stats, play spawn animation, trigger any "battlecry" effects
        public void OnSpawn(TileControl tile)
        {
            // @TODO: Setup the bodies for this creature...
            // Instantiate bodies based on the primary stat of the creature
            // Need a good way of differentiating multiple "bodies" without having to instantiate. What happens if you get a creature up to 50 power?!

            currentTile = tile;

            currentTile.occupyingTeam = team;

            currentState = CreatureState.IDLE;

            CardProperties secondary = m_card.GetCardProperty(Settings.ResourcesManager.secondaryStatElement);
            secondaryStat = secondary.intValue;

            CardProperties primary = m_card.GetCardProperty(Settings.ResourcesManager.primaryStatElement);
            primaryStat = primary.intValue;

            remainingMovement = secondaryStat;

            // Check the surrounding tiles for an enemy
            targetTile = CheckSurroundingTilesForEnemy();

            if(targetTile != null)
            {
                currentState = CreatureState.FIGHTING;
            }
            else
            {
                //Debug.Log("Creature| No enemy found on spawn, moving forvart!");
                targetTile = GetForwardTile(currentTile.row);

                if(targetTile == null)
                {
                    Debug.LogError("Creature|| SOMETHING BAD HAPPENED. FORWARD TILE IS NULL ON CREATURE SPAWN: [" + (currentTile.row + 1).ToString() + ", " + currentTile.column + "]");
                }

                currentState = CreatureState.MOVING;
            }
        }

        protected TileControl GetForwardTile(int currentRow)
        {
            TileControl forwardTile = null;

            var targetRow = currentRow + ((Settings.gameManager.localTeam == this.team) ? 1 : - 1);

            // Check forward
            forwardTile = currentTile.lane.GetTile(targetRow, currentTile.column);

            return forwardTile;

        }

        protected TileControl CheckSurroundingTilesForEnemy()
        {
            TileControl targetTile = null;
            var currentRow = currentTile.row;
            var currentColumn = currentTile.column;

            var targetColumn = currentColumn - 1;
            var targetRow = currentRow;

            // Check right
            targetTile = currentTile.lane.GetTile(targetRow, targetColumn);
            if (targetTile != null)
            {
                bool enemyOccupied = targetTile.occupyingTeam != team && targetTile.occupyingTeam != Team.None;
                if (enemyOccupied)
                {
                    //Debug.Log("Creature| Found enemy to the right!");
                    return targetTile;
                }
            }

            targetColumn = currentColumn + 1;

            // Check left
            targetTile = currentTile.lane.GetTile(targetRow, targetColumn);
            if (targetTile != null)
            {
                bool enemyOccupied = targetTile.occupyingTeam != team && targetTile.occupyingTeam != Team.None;
                if (enemyOccupied)
                {
                    //Debug.Log("Creature| Found enemy to the left!");
                    return targetTile;
                }
            }

            targetColumn = currentColumn;
            targetRow = currentRow + 1;

            // Check forward
            targetTile = currentTile.lane.GetTile(targetRow, targetColumn);
            if (targetTile != null)
            {
                bool enemyOccupied = targetTile.occupyingTeam != team && targetTile.occupyingTeam != Team.None;
                if (enemyOccupied)
                {
                    //Debug.Log("Creature| Found enemy to the front!");
                    return targetTile;
                }
            }

            //Debug.Log("Creature| No enemy found around us!");
            return null;
        }

        private void Move()
        {
            // If the tile is occupied by an enemy, stop our move and FIGHT!
            if(targetTile.occupyingTeam != team && targetTile.occupyingTeam != Team.None)
            {
                currentState = CreatureState.FIGHTING;
                return;
            }

            // If the tile is occupied by a friendly unit, stop movement and return to idle.
            if(targetTile.occupyingTeam == team)
            {
                currentState = CreatureState.IDLE;
                remainingMovement--;
                return;
            }

            // Otherwise, let's do the move...

            //Debug.Log("Creature| Move()");
            isBusy = true;

            // Reset the team of our current tile
            currentTile.occupyingTeam = Team.None;
            currentTile = targetTile;
            // Take control of the targetTile
            currentTile.occupyingTeam = team;
            
            Vector3 targetPos = currentTile.transform.position;

            // Check creature team to see if we should move the local or enemy front
            if (Settings.gameManager.localTeam == this.team)
            {
                if (currentTile.row > currentTile.lane.localTerritoryMarker.territoryRow)
                {
                    currentTile.lane.localTerritoryMarker.MoveTerritory(1);
                }
            }
            else
            {
                if (currentTile.row < currentTile.lane.enemyTerritoryMarker.territoryRow)
                {
                    currentTile.lane.enemyTerritoryMarker.MoveTerritory(-1);
                }
            }

            System.Action onFinishMovement = delegate
            {
                // When the move is done, finalize movement settings
                remainingMovement--;
                AttachToTile(currentTile.transform);
                currentState = CreatureState.IDLE;
                
                targetTile = null;
                isBusy = false;
            };

            // Do the move
            movement.Move(targetPos, onFinishMovement);
        }

        private void Fight(TileControl targetTile)
        {
            isBusy = true; // wat

            Creature targetCreature = targetTile.GetComponentInChildren<Creature>();

            if(targetCreature != null)
            {
                Debug.Log("Creature| Fight() " + targetCreature.name);
                var creatureDamage = targetCreature.primaryStat;
                var myDamage = this.primaryStat;

                targetCreature.HandleDamage(myDamage);
                this.HandleDamage(creatureDamage);
            }

            isBusy = false; // wat

            if(currentState != CreatureState.DEAD)
            {
                targetTile.occupyingTeam = Team.None;
                currentState = CreatureState.MOVING;
            }
        }

        private void AttachToTile(Transform tile)
        {
            transform.SetParent(tile);
            transform.localPosition = Vector3.zero;
            transform.rotation = Quaternion.identity;
            transform.localScale = Vector3.one;
            
        }

        public void HandleDamage(int damage)
        {
            //Debug.Log("Creature| " + gameObject.name + " has [" + primaryStat.ToString() + "] hp and is taking [" + damage.ToString() + "] damage");
            primaryStat -= damage;

            int count = damage;
            foreach(var body in bodies)
            {
                if (count == 0)
                    break;

                if(body.activeInHierarchy && count > 0)
                {
                    body.SetActive(false);
                    count--;
                }
            }

            if (primaryStat <= 0)
            {
                Die();
            }
        }

        private void Die()
        {
            currentTile.occupyingTeam = Team.None;

            // This creature just died. If it's not my turn, 
            bool isMyTurn = Settings.gameManager.IsTeamTurn(this.team);
            bool isLocal = this.team == Settings.gameManager.localTeam;
            if(!isMyTurn)
            {
                currentTile.lane.ResetTerritoryToFurthestControlledTile(isLocal);
            }

            currentState = CreatureState.DEAD;
        }
    }
}
