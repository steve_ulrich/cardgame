﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    [CreateAssetMenu(menuName = "Kalard/Areas/BoardWhenHoldingCards")]
    public class BoardAreaLogic : AreaLogic
    {
        public CardVariable card;
        public CardType creatureType;
        public CardType resourceType;
        public SO.TransformVariable areaGrid;
        public SO.TransformVariable resourceGrid;
        public GameLogic.BaseActorLogic cardDownLogic;

        public override void Execute()
        {
            if (card.value == null)
            {
                return;
            }

            Card c = card.value.uIControl.card;
            bool canUse = Settings.gameManager.currentPlayer.CanUseCard(c);
            if (canUse)
            {
                if (c.cardType == creatureType)
                {
                    System.Collections.Generic.List<UnityEngine.EventSystems.RaycastResult> results = Settings.GetUIObjects();

                    TileControl tile = null;
                    foreach (var result in results)
                    {
                        tile = result.gameObject.GetComponent<TileControl>();

                        if (tile != null)
                        {
                            break;
                        }
                    }
                    // Test if we hit a tile
                    if (tile != null)
                    {
                        // Get the lane and row of the tile
                        var tileLane = tile.lane;

                        var tileRow = tile.row;
                        var frontRow = tileLane.localTerritoryMarker.territoryRow;

                        if (tileRow > frontRow)
                        {
                            card.value.gameObject.SetActive(true);
                            Debug.Log("Trying to place a creature past our Front");
                            return;
                        }

                        SessionManager.instance.DropCreature(c, tile);


                        // Turn off the card from our hand
                        card.value.gameObject.SetActive(false);

                        return;
                    }
                }
                // @TODO: We're going to get rid of this since our resources will be incremented
                // every turn instead of being resource cards...make sure we keep some form of this
                // in case we switch to resource cards, but I doubt we will!
                if (c.cardType == resourceType)
                {
                    Settings.SetParentForCard(card.value.transform, resourceGrid.value);
                    //card.value.gameObject.SetActive(true);
                    card.value.currentLogic = cardDownLogic;

                    Settings.gameManager.currentPlayer.AddResourceCard(card.value.gameObject);
                }
                

            }
            else
            {

                //Debug.Log("Cannot use");
            }

            card.value.gameObject.SetActive(true);
        }
    }
}

