﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    public class CardInstance : MonoBehaviour, IClickable
    {
        public CardUIControl uIControl;
        public GameLogic.BaseActorLogic currentLogic;
        public PlayerHolder owner;

        public void OnClick()
        {
            if(currentLogic == null)
            {
                return;
            }
            currentLogic.OnClick(this);
        }

        public void OnHighlight()
        {
            if (currentLogic == null)
            {
                return;
            }
            currentLogic.OnHighlight(this);
        }

        public void OnStopHighlight()
        {
            if (currentLogic == null)
                return;

            currentLogic.OnStopHighlight(this);
        }
    }
}