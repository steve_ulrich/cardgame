﻿using UnityEngine;
using System.Collections;
using Kalard;

namespace Kalard.GameLogic
{
    [CreateAssetMenu(menuName = "Kalard/MyHand")]
    public class HandCard : Kalard.GameLogic.BaseActorLogic
    {
        public SO.GameEvent onCurrentCardSet;
        public CardVariable currentCard;
        public Kalard.GameStates.State holdingCardState;

        public override void OnClick(CardInstance inst)
        {
            currentCard.Set(inst);
            Settings.gameManager.SetState(holdingCardState);
            onCurrentCardSet.Raise();
        }

        public override void OnHighlight(CardInstance inst)
        {
            // Pull the card up
            inst.uIControl.PullUpCard();
        }

        public override void OnStopHighlight(CardInstance inst)
        {
            // Drop card
            inst.uIControl.SendCardBackToHand();
        }
    }
}

