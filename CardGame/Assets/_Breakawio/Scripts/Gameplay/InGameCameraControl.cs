﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Attached to our in-game camera, this script handles movement of the camera between various lanes
/// </summary>
public class InGameCameraControl : MonoBehaviour
{
    public float transitionTime = 1f;

    public Transform FullBoardView;
    public GameObject FullBoardSelector;
    public Transform LeftLaneView;
    public GameObject LeftBoardSelector;
    public Transform RightLaneView;
    public GameObject RightBoardSelector;
    public Transform MidLaneView;
    public GameObject MidBoardSelector;

    [ContextMenu("Full Board")]
    public void MoveToFullBoard()
    {
        StartCoroutine(MoveToPosition(FullBoardView.position));

        SetBoardSelectorStatus(false, true, true, true);
    }

    [ContextMenu("Left Board")]
    public void MoveToLeftBoard()
    {
        StartCoroutine(MoveToPosition(LeftLaneView.position));
        SetBoardSelectorStatus(true, false, true, true);
    }

    [ContextMenu("Mid Board")]
    public void MoveToMidBoard()
    {
        StartCoroutine(MoveToPosition(MidLaneView.position));
        SetBoardSelectorStatus(true, true, false, true);
    }

    [ContextMenu("Right Board")]
    public void MoveToRightBoard()
    {
        StartCoroutine(MoveToPosition(RightLaneView.position));
        SetBoardSelectorStatus(true, true, true, false);
    }

    public void SetBoardSelectorStatus(bool full, bool left, bool mid, bool right)
    {
        FullBoardSelector.SetActive(full);
        LeftBoardSelector.SetActive(left);
        MidBoardSelector.SetActive(mid);
        RightBoardSelector.SetActive(right);
    }

    public IEnumerator MoveToPosition(Vector3 targetPos)
    {
        Vector3 basePos = transform.position;
        Vector3 currentPos = transform.position;
        float currentTimer = 0f;

        Vector3 halfwayTarget = targetPos;
        halfwayTarget.y = (currentPos.y / 2) + currentPos.y;
        halfwayTarget.x = ((currentPos.x + targetPos.x) / 2);

        //Debug.Log("Halfway: " + halfwayTarget.ToString());
        //Debug.Log("Target: " + targetPos.ToString());

        float halfTimer = transitionTime / 2;

        while(currentTimer < halfTimer)
        {
            currentPos = Vector3.Lerp(basePos, halfwayTarget, currentTimer / halfTimer);
            currentTimer += Time.deltaTime;
            transform.position = currentPos;
            yield return new WaitForEndOfFrame();
        }

        //Debug.Log("Current (halfway): " + currentPos.ToString());

        currentTimer = 0f;

        while (currentTimer < halfTimer)
        {
            currentPos = Vector3.Lerp(halfwayTarget, targetPos, currentTimer / halfTimer);
            currentTimer += Time.deltaTime;
            transform.position = currentPos;
            yield return new WaitForEndOfFrame();
        }

    }
}
