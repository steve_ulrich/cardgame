﻿using UnityEngine;
using System.Collections;

namespace Kalard
{
    public class TerritoryMarker : MonoBehaviour
    {
        public int territoryRow;
        public Transform territoryIndicator;


        [ContextMenu("TestMoveEnemyTerritory")]
        public void TestMoveEnemyTerritory()
        {
            MoveTerritory(-2);
        }

        public void MoveTerritory(float units)
        {
            StopAllCoroutines();

            Vector3 target = Vector3.zero;

            territoryRow += (int)units;

            target = territoryIndicator.position;
            target.z += units;

            StartCoroutine(Kalard.Utility.MoveTransformAsync(territoryIndicator, target, .25f));
        }
    }
}
