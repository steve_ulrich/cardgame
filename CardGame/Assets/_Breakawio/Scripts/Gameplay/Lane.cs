﻿using UnityEngine;
using System.Collections.Generic;
using FullInspector;

namespace Kalard
{
    public class Lane : BaseBehavior
    {
        public Dictionary<int, List<TileControl>> tiles;

        public TerritoryMarker localTerritoryMarker;
        public TerritoryMarker enemyTerritoryMarker;

        protected override void Awake()
        {
            base.Awake();

            foreach (var row in tiles)
            {
                var tileList = row.Value;
                for(int i = 0; i < tileList.Count; i++)
                {
                    var tile = tileList[i];
                    tile.lane = this;
                    tile.row = row.Key;
                    tile.column = i;
                }
            }

            // Set the enemy territory starting row
            for (int row = 0; row < tiles.Count; row++)
            {
                enemyTerritoryMarker.territoryRow = row;
            }

            // Set the local team starting territory row
            localTerritoryMarker.territoryRow = 0;
        }

        public void ResetTerritoryToFurthestControlledTile(bool isLocal)
        {
            int furthestRow = 0;
            
            if (isLocal)
            {
                foreach (var row in tiles)
                {
                    var tileList = row.Value;
                    for (int i = 0; i < tileList.Count - 1; i++) // We do count - 1 instead of count because the final row cannot be a territory
                    {
                        if (tileList[i].occupyingTeam == Settings.gameManager.localTeam)
                        {
                            furthestRow = row.Key;
                            Debug.Log("LOCAL: Found FurthestRow @ " + furthestRow);
                        }
                    }
                }
                Debug.Log("Resetting local territory from: " + localTerritoryMarker.territoryRow + " to row: " + furthestRow);
                localTerritoryMarker.MoveTerritory(furthestRow - localTerritoryMarker.territoryRow);
            }
            else
            {
                for(int row = 0; row < tiles.Count; row++)
                {
                    furthestRow = row;
                }


                foreach (var row in tiles)
                {
                    var tileList = row.Value;
                    for (int i = tileList.Count - 1; i > 0; i--) // We do > 0 instead of >= 0 because the final row cannot be a territory
                    {
                        if (tileList[i].occupyingTeam != Settings.gameManager.localTeam && tileList[i].occupyingTeam != Team.None)
                        {
                            furthestRow = row.Key;
                            Debug.Log("ENEMY: Found FurthestRow @ " + furthestRow);
                        }
                    }
                }

                Debug.Log("Resetting enemy territory from: " + enemyTerritoryMarker.territoryRow + " to row: " + furthestRow);
                enemyTerritoryMarker.MoveTerritory(furthestRow - enemyTerritoryMarker.territoryRow);
            }

        }

        public TileControl GetTile(int row, int column, bool swapForEnemy = false)
        {
            TileControl tile = null;

            if(row >= tiles.Count || row < 0)
            {
                Debug.LogWarning("Lane|| Requesting a tile from a non-existant ROW [" + row + "]");
            }
            else if(column >= tiles[row].Count || column < 0)
            {
                Debug.LogWarning("Lane|| Requesting a tile from a non-existant COLUMN [" + column + "]");
            }
            else
            {
                if(swapForEnemy)
                {
                    int maxRows = tiles.Count - 1;
                    int maxColumns = tiles[row].Count - 1;

                    row = maxRows - row;
                    column = maxColumns - column;

                    tile = tiles[row][column];

                }
                else
                {
                    tile = tiles[row][column];
                }
            }

            return tile;
        }

        [ContextMenu("Fill Dict")]
        public void FillDictionary()
        {
            tiles.Clear();
            int row = 0;
            int tileNum = 0;
            while(tileNum < transform.childCount)
            {
                tiles[row] = new List<TileControl>();

                for(int i = 0; i < 5; i++)
                {
                    tiles[row].Add(transform.GetChild(tileNum).GetComponent<TileControl>());
                    tileNum++;
                }

                row++;
            }
        }
    }
}
