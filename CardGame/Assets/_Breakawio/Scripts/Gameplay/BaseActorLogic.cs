﻿using UnityEngine;
using System.Collections;

namespace Kalard.GameLogic
{
    public abstract class BaseActorLogic : ScriptableObject
    {
        public abstract void OnClick(CardInstance inst);

        public abstract void OnHighlight(CardInstance inst);

        public abstract void OnStopHighlight(CardInstance inst);
    }
}

