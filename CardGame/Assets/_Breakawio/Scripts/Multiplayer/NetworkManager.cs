﻿using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using UnityEngine;
using Photon.Pun;

namespace Kalard.Networking
{
    public enum QueueType
    {
        Solo,
        Duo
    }

    public enum MatchmakingType
    {
        Casual,
        Ranked,
        Custom
    }

    public class NetworkManager : Photon.Pun.MonoBehaviourPunCallbacks, IConnectionCallbacks
    {
        public static bool isMaster;
        public static NetworkManager instance;
        public PunLogLevel punLogLevel;
        public byte maxPerRoomSolo = 2;
        public byte maxPerRoomDuo = 4;

        public QueueType queueType;
        public MatchmakingType matchmakingType;
        ExitGames.Client.Photon.Hashtable customOptions;
        private string[] expectedUsers; // Only initialized and added to if we have friends in matchmaking

        [Space(10)]
        public SO.StringVariable logger;

        [Space(10)]
        public SO.GameEvent onConnected;
        public SO.GameEvent loggerUpdated;
        public SO.GameEvent failedToConnect;
        public SO.GameEvent waitingForPlayers;

        private int numConnected = 0;
        public UnityEngine.UI.Image[] connectedStatusImages;

        private int firstTurnIndex;

        private void Awake()
        {
            if(instance == null)
            {
                instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(this.gameObject);
            }

            expectedUsers = null;
        }

        private void Start()
        {
            PhotonNetwork.LogLevel = punLogLevel;
            PhotonNetwork.AutomaticallySyncScene = true;
            Init();
        }

        private void Init()
        {
            LogMessage("Connecting to Photon Network...");

            if(PhotonNetwork.IsConnectedAndReady)
            {

            }
            else
            {
                PhotonNetwork.ConnectUsingSettings();
            }
        }

        #region My Calls

        private void SetConnectedImages()
        {
            for(int i = 0; i < numConnected; i++)
            {
                connectedStatusImages[i].enabled = true;
            }
        }

        private void LogMessage(string message)
        {
            Debug.Log(message);
            logger.value = message;
            loggerUpdated.Raise();
        }

        public void OnPlayGame()
        {
            customOptions = new ExitGames.Client.Photon.Hashtable();

            customOptions.Add("q", queueType.ToString());
            customOptions.Add("m", matchmakingType.ToString());

            JoinRandomRoom(customOptions);
        }

        void JoinRandomRoom(ExitGames.Client.Photon.Hashtable options)
        {
            LogMessage("Joining Random Room for QueueType:" + queueType.ToString() + " and MatchMaking Type: " + matchmakingType.ToString());

            byte expectedMax = queueType == QueueType.Solo ? maxPerRoomSolo : maxPerRoomDuo;

            PhotonNetwork.JoinRandomRoom(options, expectedMax, MatchmakingMode.FillRoom, TypedLobby.Default, string.Empty, expectedUsers);
        }

        void CreateRoom()
        {
            byte expectedMax = queueType == QueueType.Solo ? maxPerRoomSolo : maxPerRoomDuo;
            LogMessage("Creating Room for QueueType:" + queueType.ToString() + " and MatchMaking Type: " + matchmakingType.ToString());
            RoomOptions room = new RoomOptions();
            room.MaxPlayers = expectedMax;
            room.CustomRoomPropertiesForLobby = new string[2] { "q", "m" };
            room.CustomRoomProperties = customOptions;

            PhotonNetwork.JoinOrCreateRoom(Utility.RandomAlphaNumericString(256), room, TypedLobby.Default);

        }
        
        #endregion  

        #region Photon Callbacks

        public override void OnConnectedToMaster()
        {
            base.OnConnectedToMaster();

            LogMessage(User.ActiveUser.DisplayName + " connection : SUCCESS");

            onConnected.Raise();
        }

        public override void OnCreatedRoom()
        {
            base.OnCreatedRoom();
            firstTurnIndex = new System.Random().Next(0, PhotonNetwork.CurrentRoom.MaxPlayers);

            LogMessage("Room Created - First turn = " + firstTurnIndex.ToString());

            isMaster = true;


            numConnected++;
            SetConnectedImages();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);

            numConnected++;
            SetConnectedImages();

            LogMessage("New Player Joined: A# " + newPlayer.ActorNumber + " playercount: " + PhotonNetwork.PlayerList.Length.ToString());

            if (PhotonNetwork.PlayerList.Length >= maxPerRoomSolo)
            {
                // Start
                LogMessage("Ready for match!");

                if (isMaster)
                {
                    PhotonNetwork.CurrentRoom.IsOpen = false;

                    // Get the index of the starting player
                    int startingId = PhotonNetwork.PlayerList[firstTurnIndex].ActorNumber;

                    object[] data = new object[1];
                    data[0] = startingId;

                    PhotonNetwork.Instantiate("SessionManager", Vector3.zero, Quaternion.identity, 0, data);

                }
            }
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();

            LogMessage("Waiting for players...");

            numConnected = PhotonNetwork.CurrentRoom.PlayerCount;
            SetConnectedImages();

            waitingForPlayers.Raise();
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);

            if(cause == DisconnectCause.CustomAuthenticationFailed)
            {
                LogMessage("Failed to connect!");

                failedToConnect.Raise();
            }
            
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            base.OnJoinRandomFailed(returnCode, message);

            LogMessage(message);

            CreateRoom();
        }

        public override void OnLeftRoom()
        {
            base.OnLeftRoom();
        }

        #endregion

        #region RPCs



        #endregion
    }

}
