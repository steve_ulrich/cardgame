﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    public class NetworkPlayer : Photon.Pun.MonoBehaviourPun, Photon.Pun.IPunInstantiateMagicCallback
    {
        public int photonId;
        public bool isLocal;
        public Team team;
        string[] cardIds;
        Dictionary<int, Card> cards = new Dictionary<int, Card>();

        public string[] GetStartingCardIds()
        {
            return cardIds;
        }

        public void OnPhotonInstantiate(Photon.Pun.PhotonMessageInfo info)
        {
            isLocal = photonView.Owner.IsLocal;
            photonId = photonView.Owner.ActorNumber;

            if(photonId % 2 == 0)
            {
                team = Team.Blue;
            }
            else
            {
                team = Team.Red;
            }

            object[] data = photonView.InstantiationData;
            cardIds = (string[])data[0];

            SessionManager.instance.AddPlayer(this);
        }


        public void RegisterCard(Card c)
        {
            cards.Add(c.instId, c);
        }

        public Card GetCard(int instId)
        {
            Card c = null;
            cards.TryGetValue(instId, out c);
            return c;
        }
    }
}

