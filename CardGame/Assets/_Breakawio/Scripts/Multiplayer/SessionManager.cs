﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kalard
{
    /// <summary>
    /// @TODO: Destroy the instance when a match/session is over
    /// </summary>
    public class SessionManager : Photon.Pun.MonoBehaviourPun, Photon.Pun.IPunInstantiateMagicCallback
    {
        public static SessionManager instance;

        Transform multiplayerReferences;
        List<NetworkPlayer> players = new List<NetworkPlayer>();
        NetworkPlayer localPlayer;

        public PlayerHolder localPlayerHolder;
        public PlayerHolder clientPlayerHolder;
        public List<PlayerHolder> playerHolders;

        private int startingIndex;

        private void Update()
        {
            if(gameSceneLoaded && !allPlayersAdded)
            {
                if (players.Count >= Networking.NetworkManager.instance.maxPerRoomSolo)
                {
                    allPlayersAdded = true;
                    StartMatch();
                }
            }
            
        }

        #region My Calls

        public void StartMatch()
        {
            GameManager gm = Settings.gameManager;
            Debug.Log("MultiplayerManager| StartMatch() -- load up number of players: " + players.Count);

            foreach (var p in players)
            {
                Debug.Log("MultiplayerManager| StartMatch() - setup player: " + p.photonId);

                if(p.isLocal)
                {
                    localPlayerHolder.gameId = p.photonId;
                    localPlayerHolder.allCards.Clear();
                    localPlayerHolder.allCards.AddRange(p.GetStartingCardIds());
                    localPlayerHolder.team = p.team;

                    playerHolders.Add(localPlayerHolder);

                    gm.localTeam = localPlayerHolder.team;
                }
                else
                {
                    PlayerHolder client = Instantiate(clientPlayerHolder) as PlayerHolder;
                    client.gameId = p.photonId;
                    client.allCards.Clear();
                    client.allCards.AddRange(p.GetStartingCardIds());
                    //@TODO Add portrait, userName...anything custom to the player
                    client.team = p.team;

                    playerHolders.Add(client);

                }
            }

            gm.allPlayers = playerHolders.ToArray();

            playerHolders.Clear();

            // Initialize the game with the first turn going to  the starting client
            gm.InitGame(startingIndex);
        }

        public void AddPlayer(NetworkPlayer n_print)
        {
            if(n_print.isLocal)
            {
                localPlayer = n_print;
            }

            players.Add(n_print);
            n_print.transform.parent = multiplayerReferences;
        }

        [Photon.Pun.PunRPC]
        public void EndTurnRPC()
        {
            Settings.gameManager.EndTurn();
        }

        public void EndCurrentPhase()
        {
            photonView.RPC("EndTurnRPC", Photon.Pun.RpcTarget.All);
        }

        [Photon.Pun.PunRPC]
        public void DropCreatureRPC(string cardId, int row, int column, string laneName, Team t)
        {
            Debug.Log("DropCreatureRPC| [" + cardId + "] dropping on [" + row + "][" + column + "] in " + laneName + " for Team [" + t.ToString() + "]");

            Card c = Settings.ResourcesManager.GetCardInstance(cardId);
            bool isEnemy = t != localPlayer.team;

            TileControl tile = null;
            switch (laneName) // @TODO: Can we make this a non-string?
            {
                case "LeftLane":
                    tile = Settings.gameManager.laneManager.leftLane.GetTile(row, column, isEnemy);
                    break;
                case "MidLane":
                    tile = Settings.gameManager.laneManager.midLane.GetTile(row, column, isEnemy);
                    break;
                case "RightLane":
                    tile = Settings.gameManager.laneManager.rightLane.GetTile(row, column, isEnemy);
                    break;
            }

            Settings.DropCreatureCard(c, tile);
        }

        public void DropCreature(Card c, TileControl tile)
        {
            int row = tile.row;
            int column = tile.column;
            string laneName = tile.lane.name;
            string cardId = c.name;
            Team t = localPlayer.team;

            //Debug.Log("LocalDropCreature| [" + cardId + "] dropping on [" + row + "][" + column + "] in " + laneName + " for Team [" + t.ToString() + "]");

            photonView.RPC("DropCreatureRPC", Photon.Pun.RpcTarget.All, cardId, row, column, laneName, t);
        }

        NetworkPlayer GetPlayer(int photonId)
        {
            for (int i = 0; i < players.Count; i++)
            {
                if (players[i].photonId == photonId)
                {
                    return players[i];
                }
            }

            Debug.LogError("MultiplayerManager| GetPlayer() couldn't find player with id: " + photonId.ToString());
            return null;
        }
        
        #endregion

        #region Init

        /// <summary>
        /// Load up the local PlayerProfile, fill our data array with its properties,
        /// then instantiate it across the network for everyone
        /// </summary>
        void InstantiateNetworkPrint()
        {
            PlayerProfile profile = Resources.Load("PlayerProfile") as PlayerProfile;
            object[] data = new object[1];
            data[0] = profile.cardIds;

            Photon.Pun.PhotonNetwork.Instantiate("NetworkPlayer", Vector3.zero, Quaternion.identity, 0, data);
        }

        public void OnPhotonInstantiate(Photon.Pun.PhotonMessageInfo info)
        {
            multiplayerReferences = new GameObject("MultiplayerReferences").transform;

            DontDestroyOnLoad(multiplayerReferences);

            object[] data = photonView.InstantiationData;
            startingIndex = (int)data[0];

            Debug.Log("startingIndex = " + startingIndex.ToString());

            instance = this;
            DontDestroyOnLoad(instance);
            LoadGameLevel();

            InstantiateNetworkPrint();

        }

        bool gameSceneLoaded = false;
        bool allPlayersAdded = false;
        public void LoadGameLevel()
        {
            LoadLevel("Game");
        }

        public void LoadMenu()
        {
            LoadLevel("MainMenu");
        }

        void LoadLevel(string level)
        {
            System.Action onLoaded = delegate
            {
                Debug.Log("Scene Loaded");
                gameSceneLoaded = true;
            };

            Breakawio.SceneManager.Instance.LoadScene(level, onLoaded);
        }

        #endregion

    }
}

