﻿public interface IRespawnable
{
    void Respawn(bool wasDeath);
}
